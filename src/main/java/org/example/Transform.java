package org.example;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class Transform {

    private final Random random = new Random();

    public void start() {
        for (int i = 0; i < 10; i++) {

            if (i == 7 && random.nextBoolean())
                throw new RuntimeException("Some exception");

            System.out.println(i);
        }
    }

}
