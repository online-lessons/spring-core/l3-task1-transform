package org.example;

import lombok.extern.java.Log;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Log
public class TransformAudience {

    @Before("execution(* org.example.Transform.start())")
    public void before() {
        log.info("This worked before start method of Transform.class");
    }

    @AfterReturning("execution(* org.example.Transform.start())")
    public void afterReturning() {
        log.info("This worked after start method of Transform.class without exceptions");
    }

    @After("execution(* org.example.Transform.start())")
    public void after() {
        log.info("This worked after start method of Transform.class");
    }

    @AfterThrowing("execution(* org.example.Transform.start())")
    public void afterThrowing() {
        log.warning("This worked after throwing");
    }
}
