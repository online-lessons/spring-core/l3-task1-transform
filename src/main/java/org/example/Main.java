package org.example;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@ComponentScan
@EnableAspectJAutoProxy
public class Main {
    public static void main(String[] args) {

        var context = new AnnotationConfigApplicationContext(Main.class);
        Transform transform = context.getBean(Transform.class);
        transform.start();

    }
}